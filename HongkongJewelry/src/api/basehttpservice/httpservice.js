export default class HttpService {

    BASE_URL = "http://houserental.kwizko.com:8081/API"

    get(url) {
        return fetch(this.BASE_URL + url);
    }
    post(obj, url) {
        return fetch(this.BASE_URL + url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }, 
            body: JSON.stringify(obj),
        });
    }
    put(obj, url) {
        return fetch(this.BASE_URL + url, {
            method: 'Put',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }, 
            body: JSON.stringify(obj),
        });
    }    
    delete(url) {
        return fetch(this.BASE_URL + url, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        });
    }
    upload(obj, url) {
        return fetch(this.BASE_URL + url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }, 
            body: obj,
        });
    }
}