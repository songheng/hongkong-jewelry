/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { Root } from 'native-base';
import Home from './src/screens/home/home';

export default class App extends React.Component {
  render() {
    return (
      <Root><Home /></Root>
    );
  }
}
